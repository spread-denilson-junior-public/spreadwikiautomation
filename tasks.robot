*** Settings ***
Documentation       Enter weekly sales into the RobotSpareBin Industries Intranet.

Library             RPA.Browser.Selenium
Library             OperatingSystem
#Library    RPA.Windows

*** Variables ***
${URL}=                 http://localhost:8000/wp-admin/options-general.php?page=updraftplus
${USER_EMAIL}=          denilson.junior@spread.com.br
${USER_PASSWORD}=       *e8n)d!(1jp6R&o9sH

*** Tasks ***
All tasks
    Open the website
    Log in
    Make backup
    Make restore
    Make delete backup

*** Keywords ***
Open the website
    Open Available Browser        ${URL}

Log in
    Input Text                    css:input[id="user_login"]   ${USER_EMAIL}
    Input Password                css:input[id="user_pass"]    ${USER_PASSWORD}
    Submit Form

Make backup
    Wait And Click Button          css:button[id="updraft-backupnow-button"]
    Click Button When Visible      css:button[class="ui-button ui-corner-all ui-widget js-tour-backup-now-button"
    Wait Until Element Is Visible  css:div[class="updraft_success_popup--message"]  40s
    Click Button When Visible      css:button[class="button updraft-close-overlay"

Make restore
    Click Element                 css:div[class="restore-button"]
    Select Checkbox               css:input[id="updraft_restore_plugins"]
    Select Checkbox               css:input[id="updraft_restore_themes"]
    Select Checkbox               css:input[id="updraft_restore_uploads"]
    Select Checkbox               css:input[id="updraft_restore_others"]
    Select Checkbox               css:input[id="updraft_restore_db"]
    Wait And Click Button         css:button[class="button button-primary updraft-restore--next-step"]
    Wait And Click Button         css:button[class="button button-primary updraft-restore--next-step"]

Make delete backup
    ${backup_table_exists}=   Is Element Visible      css:table[class="existing-backups-table wp-list-table widefat striped"]
    ${delete_button_exists}=  Is Element Visible      css:.existing-backups-table.wp-list-table.widefat.striped tbody tr:nth-last-child(1) .updraftplus-remove a      
    IF  ${backup_table_exists} == True and ${delete_button_exists} == True
        Click Link                    css:.existing-backups-table.wp-list-table.widefat.striped tbody tr:nth-last-child(1) .updraftplus-remove a
        ${delete_button_exists_two}=  Is Element Visible      css:div[aria-describedby="updraft-delete-modal"] .ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix button:nth-child(1)
        Click Element When Visible    css:div[aria-describedby="updraft-delete-modal"] .ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix button:nth-child(1)
    END
